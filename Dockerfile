FROM debian:stretch

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes

RUN apt-get update -q -q
RUN apt-get upgrade --yes --force-yes

RUN apt-get -y install \
	build-essential \
	cmake \
	git \
	python2.7 \
	nodejs \
	default-jre \
	clang

COPY emsdk-portable /emsdk-portable
COPY qtbase /qtbase

WORKDIR /emsdk-portable

RUN ./emsdk update ; \
	./emsdk install latest ; \
	./emsdk activate latest

WORKDIR /qtbase

RUN /bin/bash -c \
	"source /emsdk-portable/emsdk_env.sh ; \
	./configure -xplatform emscripten -confirm-license -opensource -developer-build -release -static -no-thread -nomake tests -nomake examples  -no-dbus -no-headersclean -no-feature-networkinterface  -system-libpng -no-ssl"

RUN /bin/bash -c \
    "source /emsdk-portable/emsdk_env.sh ; \
	make -j 4"
