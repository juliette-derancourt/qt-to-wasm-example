#!/bin/bash

# Download Emscripten
if [ ! -d "emsdk-portable" ]; then
    wget https://s3.amazonaws.com/mozilla-games/emscripten/releases/emsdk-portable.tar.gz
	tar xfz emsdk-portable.tar.gz
	rm -f emsdk-portable.tar.gz
fi

# Download the port of Qt to Emscripten (see https://bugreports.qt.io/browse/QTBUG-63917)
if [ ! -d "qtbase" ]; then
	git clone https://code.qt.io/qt/qtbase.git --branch wip/webassembly --single-branch
fi

# Build the container
docker build -t qt-to-webassembly .

# Run it
docker run -t -i -v `pwd`/qt-opengl-example:/qt-opengl-example qt-to-webassembly /bin/bash -c "source /emsdk-portable/emsdk_env.sh ; cd /qt-opengl-example ; /qtbase/qmake/qmake ; make"
