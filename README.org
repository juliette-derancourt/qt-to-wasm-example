#+TITLE: Compilation of a Qt app with emscripten

* Build and run the Docker container

See [[file:compile.sh]] which relies on [[file:Dockerfile]].
This will compile the Qt example (see [[file:qt-opengl-example/main.cpp]]) to a wasm binary, using a port of Qt to WebAssembly (https://bugreports.qt.io/browse/QTBUG-63917).

* Test the WebAssembly binary

Start a web server:
#+BEGIN_SRC sh
python -m SimpleHTTPServer
#+END_SRC

Then open http://localhost:8000 in a browser, and open the html file in qt-opengl-example.
